+++
title = "ALC Beijing Episode #10 谈谈内源2"
description = ""
tags = [
    "基础"
]
date = "2021-03-03"
categories = [
	"基础"
]
+++





## 内容梗概 ## (#内容梗概)

目前，越来越多的软件开发者把开源的优秀实践带到企业内部，在组织内部建立起了一种类似开源的文化氛围。在上期关于内源的节目中，姜宁老师和谭中意老师让我们了解了内源和开源的关系，以及内源究竟是什么。

在这一期节目中，姜宁老师，谭中意老师则对内源的话题进一步聚焦，就内源的具体适用性、责任方以及动力问题进行了详细的讨论。让我们一起听一下他们在这一期中为我们带来的精彩呈现吧。

## 时间线 ## (#时间线)

|时间区间|精彩话语提炼|
|--------|--------|
|03:10～～08:50|什么样的项目适合以内源的方式在公司内部开源|
|09:02～～13:29|内源项目应该由谁来兜底？|
|13:30～～17:56|内源项目的商业交付模式|
|18:00～～24:03|内源参与的源动力，姜宁：知识共享|
|24:52～～28:03|内源参与的源动力，谭中意：解决需求&技术影响力|
|32:20～～37:23|Apache way在内源中的应用|

## 请收听 ## (#请收听)

## 本期嘉宾 ## (#本期嘉宾)

**谭中意**： 开源社区资深爱好者， 开放原子基金会TOC副主席，Mozilla/Gnome/Apache committer。 国际内源基金会成员和布道者， 曾在多个开源大会上分享内部开源相关议题。

**潘娟**：京东数科高级DBA，Apache ShardingSphere PMC，ALC Beijing Member。

**姜宁**：Apache Member ， ALC Beijing 发起人，华为开源软件中心技术专家，前红帽软件首席软件工程师，从 2006 年开始一直从事 Apache 开源中间件项目的开发工作，2015年开始担任Apache 孵化器导师，参与孵化了多个源于中国Apache孵化器项目孵化。

**周禹任**: 北京大学社会学系在读本科生, Apache IoTDB Committer

## 相关链接 ## (#相关链接)

- 国际内源基金会官网（InnerSource Commons）：https://innersourcecommons.org
- 上期内源主题播客：https://alc-beijing.github.io/alc-site/post/podcast/episode-5-inner-source/

[&#8592;](https://alc-beijing.github.io/alc-site/post/how_to_be_a_successful_mentor/) (.left .arrow)[Top](#) (.top)
&copy; 2021 . Made with [Hugo](https://gohugo.io) using the [Tale](https://github.com/EmielH/tale-hugo/) theme.